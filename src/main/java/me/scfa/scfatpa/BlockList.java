package me.scfa.scfatpa;

import java.util.ArrayList;
import java.util.List;


public class BlockList {

    public static final int maxBlocks = 59;

    private final List<String> blockedList = new ArrayList<String>();
    private boolean hasAutoDeny = false;

    public boolean isPlayerBlocked(String pName) {
        return blockedList.contains(pName);
    }

    // returns sucess
    public boolean blockPlayer(String pName) {

        if (blockedList.size() > maxBlocks) {
            return false;
        }

        blockedList.add(pName);
        return true;
    }

    public boolean getAutoDeny() {
        return hasAutoDeny;
    }

    public void setAutoDeny(boolean v) {
        hasAutoDeny = v;
    }

    public void unblockPlayer(String pName) {
        blockedList.remove(pName);
    }
}
