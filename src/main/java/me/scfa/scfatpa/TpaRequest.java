package me.scfa.scfatpa;

import org.bukkit.Location;

public class TpaRequest {

    private final String playerName;
    private boolean accepted = false;
    private boolean ignored = true;
    private final Location originalPosition;

    public TpaRequest(String pName, Location originalPos) {

        playerName = pName;
        originalPosition = originalPos;
    }

    public String getSenderName() {
        return playerName;
    }

    public boolean getAcceptance() {
        return accepted;
    }

    public boolean getIgnored() {
        return ignored;
    }

    public void setIgnored(boolean v) {
        ignored = v;
    }

    public Location getOriginalPosition() {
        return originalPosition;
    }

    public void setAccepted(boolean v) {
        accepted = true;
    }

}
