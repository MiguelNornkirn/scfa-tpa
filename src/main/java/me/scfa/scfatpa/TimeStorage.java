package me.scfa.scfatpa;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

public class TimeStorage {
    public final String STORAGE_FOLDER_NAME = "cooldown_storage";
    private final ScfaTpa plugin;

    public TimeStorage(ScfaTpa p) {
        plugin = p;
    }

    public void startup() {
        plugin.getLogger().log(Level.INFO, "TimeStorage startup");

//	File theDir = new File(STORAGE_FOLDER);
//	if (!theDir.exists())
//	{
//	    plugin.getLogger().log(Level.INFO, "Created folder in " + STORAGE_FOLDER);
//	    theDir.mkdirs();
//	}

        File subFolder = new File(plugin.getDataFolder(), STORAGE_FOLDER_NAME);
        subFolder.mkdir();
    }

    public String formatRemainingCooldownTime(Player p) {

        String result = "Você já utilizou TPA. Espere pelo menos ";

        long rTime = plugin.getConfigManager().GetCooldownTime()
                - (System.currentTimeMillis() - plugin.getCooldownMap().get(p.getDisplayName()));

        if (rTime >= ScfaTpa.minuteInMs * 60) {
            result += rTime / ScfaTpa.hourInMs + " horas ";
        } else if (rTime >= ScfaTpa.minuteInMs) {
            result += rTime / ScfaTpa.minuteInMs + " minutos ";
        } else {
            result += rTime / ScfaTpa.secondInMs + " segundos ";
        }

        result += "antes de tentar novamente.";

        return result;
    }

    public boolean isCooldownExpired(String playerName) {

        Object longObj = plugin.getCooldownMap().get(playerName);

        if (longObj == null) { // if null, tries to pull from files
            longObj = loadCooldown(playerName);

            plugin.getCooldownMap().put(playerName, (long) longObj);
        }

        return System.currentTimeMillis() - (long) longObj > plugin.getConfigManager().GetCooldownTime();

    }

    @SuppressWarnings("deprecation")
    public void saveCooldowns() {

        plugin.getLogger().log(Level.INFO, "Saving cooldowns");

        for (String key : plugin.getCooldownMap().keySet()) {

            if (isCooldownExpired(key)) {
                plugin.getLogger().log(Level.INFO, "\"" + key + "\" doesn't need to save any data");
                continue;
            }

            plugin.getLogger().log(Level.INFO, "Trying to save cooldown of \"" + key + "\"");

            String uuid = Bukkit.getOfflinePlayer(key).getUniqueId().toString();
            plugin.getLogger().log(Level.INFO, "Their id is \"" + uuid + "\"");

//	    File f = new File(STORAGE_FOLDER + uuid + ".txt");
            File f = new File(plugin.getDataFolder(), STORAGE_FOLDER_NAME + File.separatorChar + uuid);
            f.getAbsoluteFile().getParentFile().mkdirs();

            FileWriter fw;
            try {

                fw = new FileWriter(f);

                fw.write("1.0\n");
                fw.write(plugin.getCooldownMap().get(key) + "\n");

                fw.close();

            } catch (IOException e) {
                e.printStackTrace();
                plugin.getLogger().log(Level.WARNING, "Failed saving data to '" + uuid + "'!");
            }

        }
    }

    @SuppressWarnings("deprecation")
    private long loadCooldown(String playername) {

        String uuid = Bukkit.getOfflinePlayer(playername).getUniqueId().toString();

        File f = new File(plugin.getDataFolder(), STORAGE_FOLDER_NAME + File.separatorChar + uuid);
        if (!f.exists()) {
            plugin.getLogger().log(Level.INFO, playername + " has no previously stored TPA data/cooldown");
            plugin.getLogger().log(Level.INFO, "Their id is \"" + uuid + "\"");
            return 0l;
        }
        plugin.getLogger().log(Level.INFO, playername + " will have their cooldown loaded");

        List<String> list = new ArrayList<String>();
        try {
            Scanner input = new Scanner(f);

            while (input.hasNextLine()) {
                list.add(input.nextLine());
            }

            input.close();
            f.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // index 0 is for version
        return Long.parseLong(list.get(1));
    }

}
