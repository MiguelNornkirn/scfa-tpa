package me.scfa.scfatpa;

import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class ScfaTpa extends JavaPlugin {

    public static final int secondInMs = 1000;
    public static final int minuteInMs = secondInMs * 60;
    public static final int hourInMs = minuteInMs * 60;

    private static final int tps = 20;

    private final Map<String, Long> cooldownMap = new HashMap<>();
    private final Map<String, Boolean> teleporting = new HashMap<>(); // Sender,
    private final Map<String, TpaRequest> pendingTp = new HashMap<>(); // Receiver, request
    private final Map<String, BlockList> blocks = new HashMap<>(); // Receiver, list
    private final List<String> occupiedPlayers = new ArrayList<>();

    private final TimeStorage timeStorageManager = new TimeStorage(this);
    private final Config configManager = new Config(this);

    @Override
    public void onEnable() { // server start/reloads

        configManager.startup();

        timeStorageManager.startup();
    }

    @Override
    public void onDisable() { // shutdown/reloads
        timeStorageManager.saveCooldowns();
    }

/*    public boolean isPortuguese(Player p) {
        return true;
*//*
        return p.getLocale().equals("pt_br") || p.getLocale().equals("pt_pt");
*//*
    }*/

    public Config getConfigManager() {
        return configManager;
    }

    public Map<String, Long> getCooldownMap() {
        return cooldownMap;
    }

    private boolean isPlayerOccupied(Player p) {

        for (String name : occupiedPlayers) {

            if (name.equals(p.getDisplayName())) {
                return true;
            }
        }

        return false;
    }

    private int getDistanceFromSpawn(Player p) {
        int spawnR = configManager.getSpawnRadius();

        double bSquare = p.getLocation().getX() * p.getLocation().getX();
        double cSquare = p.getLocation().getZ() * p.getLocation().getZ();

        if (bSquare * cSquare < 0) {

            double offset = (bSquare < 0) ? cSquare : bSquare;

            spawnR += offset;
            bSquare += offset;
            cSquare += offset;
        }

        double a = Math.sqrt(bSquare + cSquare);

        return (int) (a - spawnR);
    }

    // Only compares: world, x, y and z.
    private boolean areLocationsEqual(Location l1, Location l2) {
        return l1.getWorld().getName().equals(l2.getWorld().getName()) && l1.getX() == l2.getX()
                && l1.getY() == l2.getY() && l1.getZ() == l2.getZ();
    }

    private void noTpaMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.RED +
                "Este servidor não tem TPA normal, só SoftTPA. Use /softtpahelp para mais informações.");
    }

    private boolean softTpaCommand(CommandSender sender, String[] args) {

        if (sender instanceof Player) {

            Player tpSender = (Player) sender;

            if (teleporting.getOrDefault(tpSender.getDisplayName(), false)) {

                tpSender.sendMessage(ChatColor.RED + "Você já está teleportando.");

                return false;
            }

            if (args.length != 1) {
                tpSender.sendMessage(
                        ChatColor.YELLOW + "Uso: /softtpa <jogador>");

                return false;
            }

            if (!timeStorageManager.isCooldownExpired(tpSender.getDisplayName())) {

                tpSender.sendMessage(ChatColor.RED + timeStorageManager.formatRemainingCooldownTime(tpSender));
                return false;

            }

            if (tpSender.getDisplayName().equals(args[0])) {
                tpSender.sendMessage(ChatColor.RED + "Não é possível teleportar-se para si mesmo.");
                return false;
            }

            int spawnDistance = getDistanceFromSpawn(tpSender); // returns negative number if inside spawn
            if (spawnDistance < 0) {
                tpSender.sendMessage(ChatColor.RED + "Você precisa estar a pelo menos " + -spawnDistance
                        + " blocos de distância mais longe do spawn para usar esse comando.");
                return false;

            }

            Player tpReceiver = Bukkit.getPlayer(args[0]);

            if (tpReceiver == null) {
                tpSender.sendMessage(ChatColor.RED + "Esse jogador não está online.");
                return false;
            }

            if (isPlayerOccupied(tpReceiver)) {
                tpSender.sendMessage(ChatColor.RED + tpReceiver.getDisplayName() +
                        " já possui um pedido de SoftTPA pendente.");
                return false;

            }

            BlockList receiverBlocks = blocks.get(tpReceiver.getDisplayName());

            if (receiverBlocks != null) {
                if (receiverBlocks.getAutoDeny()) {

                    tpSender.sendMessage(ChatColor.RED +
                            tpReceiver.getDisplayName() + " possui AutoDeny(rejeição automática) ligado(a).");
                    return false;

                }

                if (receiverBlocks.isPlayerBlocked(tpSender.getDisplayName())) {
                    tpSender.sendMessage(ChatColor.RED + tpReceiver.getDisplayName() + " te bloqueou!");
                    return false;
                }
            }

            tpSender.sendMessage(ChatColor.YELLOW + "Esperando o pedido ser aceito. Não se mexa.");

            occupiedPlayers.add(tpReceiver.getDisplayName());
            TpaRequest request = new TpaRequest(tpSender.getDisplayName(), tpSender.getLocation());
            pendingTp.put(tpReceiver.getDisplayName(), request);

            teleporting.put(tpSender.getDisplayName(), true);

            tpReceiver.sendMessage(ChatColor.YELLOW + tpSender.getDisplayName()
                    + " lhe enviou um pedido de teleporte.");
            tpReceiver.playSound(tpReceiver.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 20);

            TextComponent msg = new TextComponent("ACEITAR");
            msg.setColor(net.md_5.bungee.api.ChatColor.GOLD);
            msg.setBold(true);
            msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/softtpaaccept"));
            msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Clique para aceitar.")
                            .color(net.md_5.bungee.api.ChatColor.GRAY)
                            .italic(true)
                            .create()));

            {
                TextComponent or = new TextComponent(" ou ");
                or.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
                or.setBold(false);
                msg.addExtra(or);
            }
            {
                TextComponent no = new TextComponent("RECUSAR");
                no.setColor(net.md_5.bungee.api.ChatColor.GOLD);
                no.setBold(true);

                no.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/softtpadeny"));
                no.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                        new ComponentBuilder("Clique para recusar.")
                                .color(net.md_5.bungee.api.ChatColor.GRAY)
                                .italic(true)
                                .create()));
                msg.addExtra(no);
            }
            {
                TextComponent qm = new TextComponent("? (clique)");
                qm.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
                qm.setBold(false);
                msg.addExtra(qm);
            }

            tpReceiver.spigot().sendMessage(msg);
            tpReceiver.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW +
                    "Opcionalmente, aceite com /softtpaaccept ou recuse com /softtpadeny.");

            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

                public void run() {

                    if (!request.getAcceptance() && request.getIgnored()) { // if not denied but still
                        // expired

                        tpSender.sendMessage(ChatColor.RED +
                                "O seu pedido de teleporte demorou muito de ser aceitado. Cancelando...");

//			    teleporting.remove(tpReceiver.getDisplayName());
                        teleporting.remove(tpSender.getDisplayName());
                        occupiedPlayers.remove(tpReceiver.getDisplayName());
                    }

                    pendingTp.remove(tpReceiver.getDisplayName());

                }

            }, configManager.getTeleportTimeout() * tps);

            return true;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private boolean softTpaAcceptCommand(CommandSender sender, String[] args) {

        if (sender instanceof Player) {

            Player tpReceiver = (Player) sender;
            TpaRequest request = pendingTp.get(tpReceiver.getDisplayName());

            if (request != null) {

                if (!request.getAcceptance()) {
                    Player tpSender = Bukkit.getPlayer(request.getSenderName());
                    request.setAccepted(true);

                    tpReceiver.sendMessage(ChatColor.GREEN + "Aceitando teleporte...");

                    tpSender.sendMessage(ChatColor.YELLOW + "Iniciando teleporte. Não se mova por mais "
                            + configManager.getTeleportDelay() + " segundos.");
                    tpSender.playSound(tpSender.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 29);

                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

                        public void run() {

                            if (!tpReceiver.isOnline() || !tpSender.isOnline()) {

                                if (!tpReceiver.isOnline() && tpSender.isOnline()) {
                                    tpSender.sendMessage(ChatColor.RED +
                                            "A pessoa que você tentou se teleportar saiu do jogo.");

                                } else if (!tpSender.isOnline() && tpReceiver.isOnline()) {
                                    tpReceiver.sendMessage(ChatColor.RED +
                                            "A que pessoa tentou se teleportar para você saiu do jogo.");
                                } else {
                                    getLogger().log(Level.INFO, "SoftTPA request cancelled");
                                }

                            } else if (areLocationsEqual(tpSender.getLocation(), request.getOriginalPosition())) {

                                tpSender.teleport(tpReceiver);
                                cooldownMap.put(tpSender.getDisplayName(), System.currentTimeMillis());

                                tpSender.sendMessage(ChatColor.GREEN +
                                        "Você se teleportou para " + tpReceiver.getDisplayName());

                                tpReceiver.sendMessage(ChatColor.GREEN +
                                        request.getSenderName() + " teleportou-se para você.");

                            } else {

                                tpSender.sendMessage(ChatColor.RED +
                                        "Você se moveu durante a preparação do teleporte. Cancelando...");

                                tpReceiver.sendMessage(ChatColor.RED + request.getSenderName()
                                        + " se moveu durante a preparação do teleporte. Cancelando...");
                            }

                            teleporting.remove(tpSender.getDisplayName());
                            occupiedPlayers.remove(tpReceiver.getDisplayName());
                        }

                    }, configManager.getTeleportDelay() * tps);

                    return true;

                }

                tpReceiver.sendMessage(
                        ChatColor.RED + "O pedido de teleporte já foi aceito.");

                return false;

            }

            tpReceiver.sendMessage(
                    ChatColor.RED + "Não há nenhum pedido de teleporte pendente.");
            return false;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private boolean softTpaDenyCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {

            Player tpReceiver = (Player) sender;
            TpaRequest request = pendingTp.get(tpReceiver.getDisplayName());

            if (request != null) {

                if (request.getAcceptance()) {

                    tpReceiver.sendMessage(ChatColor.RED + "O pedido de teleporte já foi aceito.");

                    return false;
                }

                request.setIgnored(false);
                Player tpSender = Bukkit.getPlayer(request.getSenderName());

                tpReceiver.sendMessage(ChatColor.GREEN +
                        "O pedido de teleporte de " + tpSender.getDisplayName() + " foi rejeitado.");

                tpSender.sendMessage(ChatColor.RED + "Seu pedido de teleporte foi rejeitado.");

                teleporting.remove(tpSender.getDisplayName());
                occupiedPlayers.remove(tpReceiver.getDisplayName());

                return true;

            }

            tpReceiver.sendMessage(ChatColor.RED + "Não há nenhum pedido de teleporte pendente.");
            return false;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private boolean softTpaBlockCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (args.length != 1) {
                p.sendMessage(ChatColor.YELLOW + "Uso: /softtpablock <jogador>");
                return false;
            }

            BlockList list = blocks.getOrDefault(p.getDisplayName(), new BlockList());

            if (list.isPlayerBlocked(args[0])) {

                p.sendMessage(ChatColor.RED + "Você já bloqueou esse jogador!");
                return false;

            }

            boolean result = list.blockPlayer(args[0]);
            if (!result) {

                p.sendMessage(ChatColor.RED + "Você já bloqueou pessoas demais!");
                return false;
            }

            blocks.put(p.getDisplayName(), list);
            p.sendMessage(ChatColor.GREEN + args[0] + " bloqueado(a).");
            return true;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private boolean softTpaUnblockCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (args.length != 1) {
                p.sendMessage(ChatColor.YELLOW + "Uso: /softtpablock <jogador>");

                return false;
            }

            BlockList list = blocks.getOrDefault(p.getDisplayName(), new BlockList());

            if (list.isPlayerBlocked(args[0])) {

                list.unblockPlayer(args[0]);
                blocks.put(p.getDisplayName(), list);

                p.sendMessage(ChatColor.GREEN + args[0] + " desbloqueado(a).");

                return true;

            }

            p.sendMessage(ChatColor.RED + "Você não bloqueou esse jogador.");

            return false;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private boolean softTpaSetAutoDenyCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (args.length != 1) {
                p.sendMessage(ChatColor.YELLOW + "Uso: /softtpasetautodeny <\"on\" ou \"off\">");
                return false;
            }

            boolean autoDeny;

            if (args[0].equals("on")) {
                autoDeny = true;

            } else if (args[0].equals("off")) {
                autoDeny = false;

            } else {
                p.sendMessage(ChatColor.YELLOW + "Uso: /softsettpaautodeny <\"on\" ou \"off\">");
                return false;
            }

            BlockList list = blocks.getOrDefault(p.getDisplayName(), new BlockList());
            list.setAutoDeny(autoDeny);
            blocks.put(p.getDisplayName(), list);

            p.sendMessage(ChatColor.GREEN + "AutoDeny definido como \"" + args[0] + "\".");

            return true;

        }

        System.out.println("This command isn't meant to be used by the console!");
        return true;
    }

    private void softTpaHelpCommand(CommandSender sender, String[] args) {

        String msg = "/softtpa <jogador> - Envia um pedido de teleporte a <jogador> (pode ser usado apenas uma vez a cada " +
                String.valueOf(getConfig().getInt("cooldown_minutes") / 60) +
                " horas e a mais de "
                + configManager.getSpawnRadius() + " blocos de distância do centro do mundo).\n"
                + "/softtpaaccept - Aceita um pedido de teleporte.\n" + "/softtpadeny - Nega um pedido de teleporte.\n"
                + "/softtpablock <jogador> - Impede <jogador> de te enviar novos pedidos de teleporte.\n"
                + "/softtpaunblock <jogador> - Desfaz /softtpablock em <jogador>.\n"
                + "/softtpasetautodeny <\"on\" ou \"off\"> - Impede todos os jogadores de te enviarem pedidos de teleporte(\"on\" liga e \"off\" desliga).";

        sender.sendMessage(ChatColor.YELLOW + msg);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (label.equalsIgnoreCase("tpa")) {
            noTpaMessage(sender);
            return true;
        }

        if (label.equalsIgnoreCase("tparules") || label.equalsIgnoreCase("tpahelp")) {
            noTpaMessage(sender);
            return true;
        }

        if (label.equalsIgnoreCase("softtpa")) {
            return softTpaCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpaaccept")) {
            return softTpaAcceptCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpadeny")) {
            return softTpaDenyCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpablock")) {
            return softTpaBlockCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpaunblock")) {
            return softTpaUnblockCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpasetautodeny")) {
            return softTpaSetAutoDenyCommand(sender, args);
        }

        if (label.equalsIgnoreCase("softtpahelp")) {
            softTpaHelpCommand(sender, args);
            return true;
        }

        System.out.println("Something really bad related to commands may have happened on " + this.getName());

        return false;

    }

}
