package me.scfa.scfatpa;

import me.scfa.scfatpa.ScfaTpa;

import java.util.logging.Level;

public class Config {


    private final ScfaTpa plugin;

    private int cooldown; // ms
    private int cancelCooldown; // seconds
    private int teleportDelay; // seconds
    private long teleportTimeout; // seconds
    private int spawnRadius; // blocks

    public Config(ScfaTpa p) {
        plugin = p;
    }

    public void startup() {
        plugin.getLogger().log(Level.INFO, "Config manager startup");
        plugin.saveDefaultConfig();
        refreshConfig();
    }

    public int GetCooldownTime() {
        return cooldown;
    }

    public int GetCancelCooldownTime() {
        return cancelCooldown;
    }

    public int getTeleportDelay() {
        return teleportDelay;
    }

    public int getSpawnRadius() {
        return spawnRadius;
    }

    public long getTeleportTimeout() {
        return teleportTimeout;
    }

    private void refreshConfig() {
        plugin.reloadConfig();

        cooldown = plugin.getConfig().getInt("cooldown_minutes") * ScfaTpa.minuteInMs;
        cancelCooldown = plugin.getConfig().getInt("cancel_cooldown_minutes");
        teleportDelay = plugin.getConfig().getInt("teleport_delay");
        teleportTimeout = plugin.getConfig().getInt("teleport_timeout");
        spawnRadius = plugin.getConfig().getInt("spawn_radius");
    }
}
